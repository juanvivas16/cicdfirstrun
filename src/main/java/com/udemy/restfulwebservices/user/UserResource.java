package com.udemy.restfulwebservices.user;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder; 



import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {

	@Autowired
	private UserDaoService service;
	
	//GET /users
	//retrieveAllUsers 
	
	@GetMapping("/users")
	public List<User> retrieveAllUsers(){
		return  service.findAll(); 
	}
	
	//GET /users/{id} 
	//retrieveUser(int id)
	
	@GetMapping("/users/{id}")
	public Resource<User> retrieveUser(@PathVariable int id) {
		
		User user = service.findObne(id);
		if(user==null){
			throw new UserNotFoundException("id-"+ id); 
		}
		
		//Se aplicara HATEOAS en este endpoint
		//Para retornar otra informacion importante junto con la respuesta
		//HATEOAS = Hypermedia As The Engine Of ApplicationStatus 
		//"all-users', server_path + "/users"	
		Resource<User> resource = new Resource<User>(user);
		//Con un static import no es necesario llamar a la clase.. 
		//Se pueden usar los metodos.. 
		
		ControllerLinkBuilder linkTo = 
				linkTo(methodOn(this.getClass()).retrieveAllUsers()); 
		
		resource.add(linkTo.withRel("all-users"));
		
		return resource;
	}
	
	//CREATED
	//input - details of user
	//output - CREATED & Return the created URI
	
	@PostMapping("/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
		User savedUser = service.save(user);
		// How to return a created status
		//   How to set the uri of the created resource in to the response
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();
		
		return ResponseEntity.created(location).build(); 
	}
		
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable int id) {
		User user = service.deleteById(id);
		if(user==null){
			throw new UserNotFoundException("id-"+ id); 
		}
	}
	
	
	
	
	
	
	
	
}
