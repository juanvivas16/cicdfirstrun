package com.udemy.restfulwebservices.helloworld;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

//controller
@RestController
public class HelloWorldController {
	
	@Autowired
	private MessageSource messageSource;
	
	/**
	GET define what method
	URI - /hello-world define to uri to access it 
	method- "Hello World" **/ 

	/**
	otraForma es:
	@RequestMapping(method=RequestMethod.GET, path="/hello-world")
	**/
	
	@GetMapping(path="/hello-world")
	public String helloWorld() {
		return "Hello World";
	}
	
	//hello-world-bean
	@GetMapping(path="/hello-world-bean")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("Hello World"); 
	}
	
	//hello-world-path-variable
	@GetMapping(path="/hello-world/path-variable/{name}")
	public HelloWorldBean helloWorldBeanPathVariable(@PathVariable String name) {
		return new HelloWorldBean(String.format("Hello world, %s", name)); 
	}
	
	
	
	/**  primera forma de hacer la internalizacion
	@GetMapping(path="/hello-world-internationalized")
	public String helloWorldInternationalized(@RequestHeader(name="Accept-Language", required=false) Locale locale) {
		return messageSource.getMessage("good.morning.message", null, locale);
	} **/
	
	
		
	
	@GetMapping(path="/hello-world-internationalized")
	public String helloWorldInternationalized() {
		
		String test; 
		
		return messageSource.getMessage("good.morning.message", null,LocaleContextHolder.getLocale() );
	}
	
	

}
