package com.udemy.restfulwebservices.helloworld;

public class HelloWorldBean {

	String message; 
	
	public HelloWorldBean(String message) {
		this.message = message;
	}

	//sin esto no se podria retornar la informacion 
	
	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "HelloWorldBean [message=" + message + "]";
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
